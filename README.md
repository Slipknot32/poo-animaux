# intro-poo


## Consignes

Créer une application qui créée un animal à chaque clic du bouton. 
Cet objet doit être représenté par une simple boite HTML contenant le nom de l'animal.

- Ne rien modifier dans app.js
- Ne travailler que dans Animal.js